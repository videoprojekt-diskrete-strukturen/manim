from manim import (
    # Scene
    MovingCameraScene,
    # Sides
    LEFT,
    RIGHT,
    # Colours
    BLACK,
    BLUE,
    WHITE,
    # Objects
    Ellipse,
    Graph,
    Group,
    # Actions
    Create,
    Transform,
    Restore,
    FadeIn,
)


class Menger(MovingCameraScene):
    VERTEX_CONFIG: dict = {
        'color': BLACK,
        'radius': 0.25,
    }
    EDGE_CONFIG: dict = {
        'color': BLACK,
    }
    GRAPH_SCALE: float = 0.6

    def construct(self):
        self.camera.background_color = WHITE

        vertices: list = list(range(21))
        edges: list[tuple[int, int]] = [
            (0, 3),
            (1, 3),
            (1, 4),
            (1, 5),
            (2, 5),
            (3, 7),
            (4, 5),
            (4, 8),
            (5, 6),
            (5, 9),
            (7, 8),
            (8, 10),
            (8, 11),
            (9, 10),
            (10, 11),
            (10, 12),
            (10, 13),
            (11, 12),
            (11, 16),
            (11, 17),
            (12, 17),
            (13, 18),
            (13, 19),
            (14, 18),
            (15, 18),
            (18, 19),
            (19, 20),
        ]
        layout: dict[int, list[int]] = {
            0: [-7, 5, 0],
            1: [-7, 0, 0],
            2: [-7, -4, 0],
            3: [-4, 3, 0],
            4: [-3, 0, 0],
            5: [-4, -2, 0],
            6: [-4, -5, 0],
            7: [-1, 5, 0],
            8: [0, 2, 0],
            9: [-1, -4, 0],
            10: [1, 0, 0],
            11: [2, 2, 0],
            12: [3, 0, 0],
            13: [2, -3, 0],
            14: [2, -5, 0],
            15: [4, -5, 0],
            16: [5, 5, 0],
            17: [6, 3, 0],
            18: [6, -4, 0],
            19: [7, 1, 0],
            20: [8, -4, 0],
        }
        # Graph, default
        graph = Graph(
            vertices,
            edges,
            edge_config=self.EDGE_CONFIG,
            vertex_config=self.VERTEX_CONFIG,
            layout=layout,
        )
        graph.scale(self.GRAPH_SCALE)

        # Graph, highlighted vertices
        graph_vertices = Graph(
            vertices,
            edges,
            edge_config=self.EDGE_CONFIG,
            vertex_config=self.VERTEX_CONFIG | {'color': BLUE},
            layout=layout,
        )
        graph_vertices.scale(self.GRAPH_SCALE)

        # Graph, highlighted edges
        graph_edges = Graph(
            vertices,
            edges,
            edge_config=self.EDGE_CONFIG | {'color': BLUE},
            vertex_config=self.VERTEX_CONFIG,
            layout=layout,
        )
        graph_edges.scale(self.GRAPH_SCALE)

        # Ellipses for highlighting sets of vertices
        ellipse1 = Ellipse(
            width=4 * self.GRAPH_SCALE,
            height=11 * self.GRAPH_SCALE,
            color='#F59B25',
            fill_opacity=0.08,
        ).move_to([c * self.GRAPH_SCALE for c in (-3.5, -1, 0)])
        ellipse2 = Ellipse(
            width=4 * self.GRAPH_SCALE,
            height=12 * self.GRAPH_SCALE,
            color='#007AFE',
            fill_opacity=0.08,
        ).move_to([c * self.GRAPH_SCALE for c in (6, 0.5, 0)])

        # Group, Graph + Ellipses
        highlight_group = Group(graph, ellipse1, ellipse2)

        # SCENE INSTRUCTIONS
        # Create Graph
        self.play(Create(graph, run_time=3))
        self.wait(5)

        # Highlight vertices temporarily
        graph.save_state()
        self.play(Transform(graph, graph_vertices, run_time=0.5))
        self.wait(3)
        self.play(Restore(graph, run_time=0.5))
        self.wait(3)

        # Highlight edges temporarily
        graph.save_state()
        self.play(Transform(graph, graph_edges, run_time=0.5))
        self.wait(3)
        self.play(Restore(graph, run_time=0.5))
        self.wait(3)

        # Highlight sets of vertices
        self.play(Create(ellipse1), Create(ellipse2))
        self.wait(3)

        # Zoom out
        self.play(self.camera.frame.animate.set(width=23, run_time=3))
        self.wait(1)

        # Duplicate graph
        self.play(highlight_group.animate.move_to(LEFT * 6))
        self.play(FadeIn(highlight_group.copy().move_to(RIGHT * 6)))
        self.wait(3)
