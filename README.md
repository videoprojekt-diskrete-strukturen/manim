# Videoprojekt Diskrete Strukturen

## Verwendeter Graph

![Mit Zahlen beschrifteter Graph](docs/graph_numbering.png)

Oben zu sehen der Graph, der für dieses Projekt verwendet wird.
Die Zahlen an den Kanten entsprechen denen der `vertices` Variable.
